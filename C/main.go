package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	arraySize = 10
	bound     = 5
)

var (
	seed   = rand.NewSource(time.Now().UnixNano())
	random = rand.New(seed)
	wg     sync.WaitGroup
)

func newArray() []int {
	var arr []int
	for i := 0; i < arraySize; i++ {
		arr = append(arr, random.Intn(bound))
	}
	return arr
}

func findSum(array []int) int {
	var sum = 0
	for i := 0; i < arraySize; i++ {
		sum += array[i]
	}
	return sum
}

func change(array []int) {
	index := random.Intn(arraySize)
	op := random.Intn(2)
	switch op {
	case 0:
		if array[index] > -1*bound {
			array[index] -= 1
		} else {
			array[index] += 1
		}
	case 1:
		if array[index] < bound {
			array[index] += 1
		} else {
			array[index] -= 1
		}
	}
}

func main() {
	var arrays [][]int

	for i := 0; i < 3; i++ {
		arrays = append(arrays, newArray())
	}

	for {
		sum := [3]int{findSum(arrays[0]), findSum(arrays[1]), findSum(arrays[2])}

		if sum[0] == sum[1] && sum[1] == sum[2] {
			for i := 0; i < 3; i++ {
				fmt.Printf("%v. %v = %v\n", i+1, arrays[i], sum[i])
			}
			break
		}

		wg.Add(3)

		for i := 0; i < 3; i++ {
			go change(arrays[i])
			fmt.Printf("%v. %v = %v\n", i+1, arrays[i], findSum(arrays[i]))
			wg.Done()
		}

		print("\n")

		wg.Wait()
	}
}
