import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Rookie implements Runnable {
    private static final AtomicBoolean done = new AtomicBoolean(false);
    private static final AtomicInteger withoutChange = new AtomicInteger(0);
    private final Barrier barrier;
    private final int[] rookies;
    private int[] previous;
    private final int leftIndex;
    private final int rightIndex;
    private final int numberOfParts;

    public Rookie(Barrier barrier, int[] rookies, int leftIndex, int rightIndex, int numberOfParts) {
        this.barrier = barrier;
        this.rookies = rookies;
        this.previous = rookies;
        this.leftIndex = leftIndex;
        this.rightIndex = rightIndex;
        this.numberOfParts = numberOfParts;
    }

    public StringBuilder printRookies() {
        StringBuilder result = new StringBuilder();
        for (int rookie : rookies) {
            switch (rookie) {
                case -1 -> result.append("< ");
                case 1 -> result.append("> ");
            }
        }
        return result;
    }

    @Override
    public void run() {
        while (!done.get()) {
            boolean change = false;

            for (int i = leftIndex; i < rightIndex; i++) {
                if (previous[i] == 1 && i + 1 < previous.length) {
                    if (previous[i + 1] == -1) {
                        rookies[i] = -1;
                        rookies[i + 1] = 1;
                        i++;
                        change = true;
                    }
                }
            }

            if (!change) {
                if (withoutChange.incrementAndGet() == numberOfParts) {
                    done.set(true);
                    previous = rookies;
                }
            }

            try {
                barrier.await();
                if (leftIndex == 0 && !done.get()) System.out.println(printRookies());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            if(!change) withoutChange.decrementAndGet();
        }
    }
}
