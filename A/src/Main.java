import java.security.SecureRandom;

public class Main {
    private static final SecureRandom random = new SecureRandom();
    private static final int numberOfRookies = 125;
    private static final int[] rookies = new int[numberOfRookies];

    private static void fillRookiesArray() {
        for (int i = 0; i < numberOfRookies; i++) {
            if (random.nextBoolean()) rookies[i] = 1;
            else rookies[i] = -1;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        fillRookiesArray();
        int numberOfParts = numberOfRookies / 50;
        int rookiesInOnePart = numberOfRookies / numberOfParts;
        int biggerParts = numberOfRookies - (rookiesInOnePart * numberOfParts);
        Barrier barrier = new Barrier(numberOfParts);
        Thread[] parts = new Thread[numberOfParts];

        int leftIndex = 0;
        for (int i = 0; i < parts.length; i++) {
            int rightIndex = leftIndex + rookiesInOnePart;
            if (i < biggerParts) {
                rightIndex++;
            }
            parts[i] = new Thread(new Rookie(barrier, rookies, leftIndex, rightIndex, numberOfParts));
            leftIndex = rightIndex;
        }

        StringBuilder firstLine = new StringBuilder();
        for (int rookie : rookies) {
            switch (rookie) {
                case -1 -> firstLine.append("< ");
                case 1 -> firstLine.append("> ");
            }
        }

        System.out.println(firstLine);

        for (Thread part : parts) part.start();

        for (Thread part : parts) part.join();
    }
}