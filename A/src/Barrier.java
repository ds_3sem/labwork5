public class Barrier {
    private final int parties;
    private int currentAwait = 0;

    public Barrier(int parties) {
        this.parties = parties;
    }

    public synchronized void await() throws InterruptedException {
        currentAwait++;

        if (currentAwait < parties) {
            this.wait();
        }

        currentAwait = 0;

        notifyAll();
    }
}
