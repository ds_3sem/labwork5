import java.security.SecureRandom;
import java.util.concurrent.CyclicBarrier;

public class Main {
    private static final int strLength = 10;
    public static void main(String[] args) throws InterruptedException {
        CyclicBarrier barrier = new CyclicBarrier(4);
        SecureRandom random = new SecureRandom();
        Thread[] threads = new Thread[4];

        char[] chars = {'A', 'B', 'C', 'D'};
        for (int i = 0; i < 4; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int j = 0; j < strLength; j++) {
                stringBuilder.append(chars[random.nextInt(4)]);
            }
            threads[i] = new Thread(new StringChanger(barrier, stringBuilder.toString(), i));
        }

        for (Thread thread : threads) thread.start();

        for (Thread thread : threads) thread.join();
    }
}