import java.security.SecureRandom;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;

public class StringChanger implements Runnable {
    private static final SecureRandom random = new SecureRandom();
    private final CyclicBarrier barrier;
    private static final AtomicBoolean done = new AtomicBoolean(false);
    private String str;
    private final int strNumber;
    private static final int[] AB = new int[4];

    public StringChanger(CyclicBarrier barrier, String str, int strNumber) {
        this.barrier = barrier;
        this.str = str;
        this.strNumber = strNumber;
    }

    public char changeTo(char forChange) {
        char toReturn = 'E';
        switch (forChange) {
            case 'A' -> toReturn = 'C';
            case 'C' -> toReturn = 'A';
            case 'B' -> toReturn = 'D';
            case 'D' -> toReturn = 'B';
        }
        return toReturn;
    }

    public int countAB() {
        int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'A' || str.charAt(i) == 'B') counter++;
        }
        return counter;
    }

    public boolean areEqual() {
        return (AB[0] == AB[1] && AB[1] == AB[2]) || (AB[0] == AB[1] && AB[1] == AB[3]) || (AB[0] == AB[3] && AB[3] == AB[2]) || (AB[3] == AB[1] && AB[1] == AB[2]);
    }

    @Override
    public void run() {
        while(!done.get()) {
            int randIndex = random.nextInt(str.length());
            char changeTo = changeTo(str.charAt(randIndex));

            StringBuilder changedStr = new StringBuilder(str);
            changedStr.setCharAt(randIndex, changeTo);

            str = changedStr.toString();

            int ABs = countAB();
            AB[strNumber] = ABs;

            try {
                for (int i = 0; i < 4; i++) {
                    if (i == strNumber) System.out.println(strNumber + 1 + ". " + str + " A + B = " + ABs);
                    barrier.await();
                }
                if (strNumber == 0) System.out.println();
                if (areEqual()) {
                    done.set(true);
                }
                Thread.sleep(50);
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
